﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Infrastructure.Repositories
{
    public abstract class BaseRepository<T> : IDisposable, IRepository<T> where T : Entity
    {
        private readonly DbContext _context;
        private readonly DbSet<T> _dbSet;
        private IQueryable<T> _currentQuery;

        protected BaseRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
            _currentQuery = _dbSet;
        }

        public IQueryable<T> GetQueryable()
        {
            return _dbSet;
        }

        public T GetById(int id)
        {
            return _currentQuery.FirstOrDefault(x => x.Id == id);
        }

        public T Add(T entity)
        {
            _dbSet.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public ICollection<T> Find(Expression<Func<T, bool>> predicate)
        {
            return _currentQuery.Where(predicate).ToList();
        }

        public IRepository<T> Include<T2>(Expression<Func<T, IEnumerable<T2>>> expression, params Expression<Func<T2, object>>[] thenExpressions)
        {
            if (thenExpressions.Length == 0)
            {
                _currentQuery = _currentQuery.Include(expression);
                return this;
            }

            var include = _currentQuery.Include(expression).ThenInclude(thenExpressions[0]);

            for (int i = 1; i < thenExpressions.Length; i++)
            {
                include =  include.Include(expression).ThenInclude(thenExpressions[i]);
            }

            _currentQuery = include;

            return this;
        }

        public async Task<T> GetByIdAsync(int id, CancellationToken cancellationToken)
        {
            return await _currentQuery.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
