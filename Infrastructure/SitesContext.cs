﻿using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public sealed class SitesContext : DbContext
    {
        public SitesContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Организаторы
        /// </summary>
        public DbSet<Organization> Organizations { get; set; }

        /// <summary>
        /// Квесты
        /// </summary>
        public DbSet<Quest> Quests { get; set; }

        /// <summary>
        /// Бронирования
        /// </summary>
        public DbSet<Reservation> Reservations { get; set; }

        /// <summary>
        /// Промокоды
        /// </summary>
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(SitesContext).Assembly);
        }
    }
}
