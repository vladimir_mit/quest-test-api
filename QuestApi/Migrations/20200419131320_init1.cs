﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuestApi.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateSequence(
                name: "CitySequence",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "organizationSequence",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "questSequence",
                incrementBy: 10);

            migrationBuilder.CreateSequence(
                name: "reservationSequence",
                incrementBy: 10);

            migrationBuilder.CreateTable(
                name: "City",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Postal = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    TimeZone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_City", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Organizations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    DisplayName = table.Column<string>(nullable: true),
                    UniqueName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organizations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PromoCodes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PromoCodes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Quests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Summary_QuestName = table.Column<string>(nullable: true),
                    Summary_PartiсipantsInfo = table.Column<string>(nullable: true),
                    Summary_DurationInfo = table.Column<string>(nullable: true),
                    Summary_CostInfo = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: false),
                    CityId = table.Column<int>(nullable: true),
                    OrganizationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Quests_City_CityId",
                        column: x => x.CityId,
                        principalTable: "City",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Quests_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reservations",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    DateTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    ParticipantName = table.Column<string>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    CostInformation_ParticipantsCount = table.Column<int>(nullable: true),
                    CostInformation_DayOfWeekNumber = table.Column<string>(nullable: true),
                    CostInformation_Time = table.Column<TimeSpan>(nullable: true),
                    CostInformation_PromoCodeId = table.Column<int>(nullable: true),
                    Cost = table.Column<int>(nullable: false),
                    ParticipantComment = table.Column<string>(nullable: true),
                    QuestId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reservations_Quests_QuestId",
                        column: x => x.QuestId,
                        principalTable: "Quests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reservations_PromoCodes_CostInformation_PromoCodeId",
                        column: x => x.CostInformation_PromoCodeId,
                        principalTable: "PromoCodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SliderPhoto",
                columns: table => new
                {
                    RelevantUrl = table.Column<string>(nullable: false),
                    OrganizationId = table.Column<int>(nullable: true),
                    QuestId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SliderPhoto", x => x.RelevantUrl);
                    table.ForeignKey(
                        name: "FK_SliderPhoto_Organizations_OrganizationId",
                        column: x => x.OrganizationId,
                        principalTable: "Organizations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SliderPhoto_Quests_QuestId",
                        column: x => x.QuestId,
                        principalTable: "Quests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_City_Name",
                table: "City",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_City_Postal",
                table: "City",
                column: "Postal",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_DisplayName",
                table: "Organizations",
                column: "DisplayName");

            migrationBuilder.CreateIndex(
                name: "IX_Organizations_UniqueName",
                table: "Organizations",
                column: "UniqueName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quests_CityId",
                table: "Quests",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Quests_OrganizationId",
                table: "Quests",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_Email",
                table: "Reservations",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_PhoneNumber",
                table: "Reservations",
                column: "PhoneNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_QuestId",
                table: "Reservations",
                column: "QuestId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservations_CostInformation_PromoCodeId",
                table: "Reservations",
                column: "CostInformation_PromoCodeId");

            migrationBuilder.CreateIndex(
                name: "IX_SliderPhoto_OrganizationId",
                table: "SliderPhoto",
                column: "OrganizationId");

            migrationBuilder.CreateIndex(
                name: "IX_SliderPhoto_QuestId",
                table: "SliderPhoto",
                column: "QuestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reservations");

            migrationBuilder.DropTable(
                name: "SliderPhoto");

            migrationBuilder.DropTable(
                name: "PromoCodes");

            migrationBuilder.DropTable(
                name: "Quests");

            migrationBuilder.DropTable(
                name: "City");

            migrationBuilder.DropTable(
                name: "Organizations");

            migrationBuilder.DropSequence(
                name: "CitySequence");

            migrationBuilder.DropSequence(
                name: "organizationSequence");

            migrationBuilder.DropSequence(
                name: "questSequence");

            migrationBuilder.DropSequence(
                name: "reservationSequence");
        }
    }
}
