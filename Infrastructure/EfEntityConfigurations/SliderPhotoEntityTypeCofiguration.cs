﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    class SliderPhotoEntityTypeCofiguration : IEntityTypeConfiguration<SliderPhoto>
    {
        public void Configure(EntityTypeBuilder<SliderPhoto> builder)
        {
            builder.HasKey(x => x.RelevantUrl);
        }
    }
}
