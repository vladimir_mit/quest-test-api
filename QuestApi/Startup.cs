using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates.CityAggregate;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Infrastructure;
using Infrastructure.Repositories.Sites;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace QuestApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("Develop",
                builder =>
                {
                    builder
                    .WithOrigins("http://localhost:3000", "http://176.193.99.111:3000")
                        .AllowAnyMethod()
                        .AllowAnyHeader(); 
                });
            });

            services.AddDbContext<SitesContext>(builder =>
            {
                builder.UseSqlServer(Configuration.GetValue<string>("SitesDb"),
                    optionsBuilder => { optionsBuilder.MigrationsAssembly("QuestApi"); });
            });

            services.AddTransient<IRepository<Organization>, OrganizationRepository>();
            services.AddTransient<IRepository<City>, CityRepository>();
            services.AddTransient<IRepository<Quest>, QuestRepository>();

            services.AddMediatR(typeof(Startup));
            services.AddAutoMapper(typeof(Startup));

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("Develop");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
