﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates;
using Domain.Sites.Model.Aggregates.CityAggregate;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using QuestApi.Sites.Features.Organizations.DTO;

namespace QuestApi.Sites.Features.Organizations
{
    public class GetById
    {
        public class Query : IRequest<Response>
        {
            public Query(int id)
            {
                Id = id;
            }

            public int Id { get; private set; }
        }

        public class Response
        {
            public string DisplayName { get; set; }

            public string Description { get; set; }

            public ICollection<SliderPhoto> SliderPhotos { get; set; }

            public ICollection<QuestSummaryDTO> QuestSummaries { get;  set; }

            public string GoogleMapSrc { get; set; }

            public bool IsGalleryEnable { get; set; }
        }

        public class Handler : IRequestHandler<Query, Response>
        {
            private readonly IRepository<Organization> _organizationRepository;
            private readonly IRepository<Quest> _questRepository;
            private readonly IMapper _mapper;
            private readonly IRepository<City> _cityRepository;

            public Handler(IRepository<Organization> organizationRepository, IRepository<City> cityRepository,
                IRepository<Quest> questRepository, IMapper mapper)
            {
                _questRepository = questRepository;
                _mapper = mapper;
                _cityRepository = cityRepository;
                _organizationRepository = organizationRepository;
            }

            public async Task<Response> Handle(Query request, CancellationToken cancellationToken)
            {
                //var org = new Organization("description", "isolation-project", "Isolation Project", new List<SliderPhoto> { new SliderPhoto("img/isolation/1") }, new List<Quest>());

                //_organizationRepository.Add(org);

                //var city = new City("0007", "Moscow", "UTC +3", new List<Quest>());
                //_cityRepository.Add(city);

                //_questRepository.Add(new Quest(org.Id,
                //    new QuestSummary("TestQuest1", "2-5 участника", "60 минут", "от 750р/ч"),
                //    "Description", city, new List<SliderPhoto>() { new SliderPhoto("img/isolation/q1/1")}));

                var organization = await _organizationRepository
                    .Include(x => x.Quests, x => x.City, x => x.Summary.MainPhoto)
                    .Include(x => x.SliderPhotos)
                    .GetByIdAsync(request.Id, cancellationToken);

                return _mapper.Map<Response>(organization); ;
            }
        }
    }
}
