﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    class QuestEntityTypeConfiguration : IEntityTypeConfiguration<Quest>
    {
        public void Configure(EntityTypeBuilder<Quest> builder)
        {
            builder.HasKey(quest => quest.Id);

            builder.Property(quest => quest.Id)
                .UseHiLo("questSequence");
            builder.Property(quest => quest.Description)
                .IsRequired();

            builder.HasMany(oo => oo.SliderPhotos)
                .WithOne();

            builder.HasOne(quest => quest.City).WithMany(c => c.Quests);

            builder.OwnsOne(oo => oo.Summary);
        }
    }
}
