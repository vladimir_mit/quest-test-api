﻿using System.Collections.Generic;
using Domain.Model.Aggregates;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates.CityAggregate;

namespace Domain.Sites.Model.Aggregates.QuestAggregate
{
    /// <summary>
    /// Квест
    /// </summary>
    public class Quest : Entity
    {
        private readonly List<SliderPhoto> _sliderPhotos;

        private Quest()
        {
            _sliderPhotos = new List<SliderPhoto>();
        }

        public Quest(int organizationId, QuestSummary summary, string description, City city, ICollection<SliderPhoto> sliderPhotos)
        {
            OrganizationId = organizationId;
            Summary = summary;
            Description = description;
            City = city;
            _sliderPhotos = new List<SliderPhoto>(sliderPhotos);
            Reservations = new List<Reservation>();
        }

        /// <summary>
        /// Основная инфомарция
        /// </summary>
        public QuestSummary Summary { get; private set; }

        /// <summary>
        /// Описание квеста
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Город
        /// </summary>
        public City City { get; private set; }

        /// <summary>
        /// Перечень бронирований
        /// </summary>
        public ICollection<Reservation> Reservations { get; private set; }

        public ICollection<SliderPhoto> SliderPhotos => _sliderPhotos;

        public int OrganizationId { get; private set; }

        public bool AddReservation(Reservation reservation)
        {
            Reservations.Add(reservation);
            return true;
        }
    }
}
