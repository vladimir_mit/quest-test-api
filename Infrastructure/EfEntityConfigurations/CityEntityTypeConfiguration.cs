﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates;
using Domain.Sites.Model.Aggregates.CityAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EfEntityConfigurations
{
    class CityEntityTypeConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Name);
            builder.HasIndex(x => x.Postal)
                .IsUnique();
            
            builder.Property(x => x.Id)
                .UseHiLo("CitySequence");

            builder.Property(x => x.Postal).IsRequired();
            builder.Property(x => x.Name).IsRequired();
        }
    }
}
