﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;

namespace Domain.Sites.Services
{
    /// <summary>
    /// Сервис подсчёта цены
    /// </summary>
    public interface IReservationCostCountService
    {
        /// <summary>
        /// Подсчитать стоимость
        /// </summary>
        /// <param name="questId"> Id квеста</param>
        /// <param name="costCountInfo"> Информация для расчёта цены </param>
        /// <returns></returns>
        int CountCost(int questId, ReservationCostInformation costCountInfo);
    }
}
