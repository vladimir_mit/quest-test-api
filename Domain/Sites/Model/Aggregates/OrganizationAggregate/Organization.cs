﻿using System.Collections.Generic;
using Domain.Model.Aggregates;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates.QuestAggregate;

namespace Domain.Sites.Model.Aggregates.OrganizationAggregate
{
    /// <summary>
    /// Организация
    /// </summary>
    public class Organization : Entity
    {
        private readonly List<Quest> _quests;
        private readonly List<SliderPhoto> _sliderPhotos;

        private Organization(string googleMapSrc)
        {
            GoogleMapSrc = googleMapSrc;
            _quests = new List<Quest>();
            _sliderPhotos = new List<SliderPhoto>();
        }

        public Organization(string description, string uniqueName, string displayName, ICollection<SliderPhoto> sliderPhotos, ICollection<Quest> quests, string googleMapSrc)
        {
            _quests = new List<Quest>(quests);
            Description = description;
            UniqueName = uniqueName;
            DisplayName = displayName;
            GoogleMapSrc = googleMapSrc;
            _sliderPhotos = new List<SliderPhoto>(sliderPhotos);
        }

        /// <summary>
        /// Список информации о квестах организатора
        /// </summary>
        public IReadOnlyCollection<Quest> Quests => _quests.AsReadOnly();

        /// <summary>
        /// Описание организатора
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// URL фото для слайдера 
        /// </summary>
        public IReadOnlyCollection<SliderPhoto> SliderPhotos => _sliderPhotos.AsReadOnly();

        /// <summary>
        /// Отображаемое имя организации
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// Уникальное имя организатора
        /// </summary>
        public string UniqueName { get; private set; }

        /// <summary>
        /// Ссылка для привязки карт
        /// </summary>
        public string GoogleMapSrc { get; private set; }
    }
}
