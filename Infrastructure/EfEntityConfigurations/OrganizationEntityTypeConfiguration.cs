﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    class OrganizationEntityTypeConfiguration : IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.HasKey(organization => organization.Id);
            builder.HasIndex(organization => organization.UniqueName)
                .IsUnique();
            builder.HasIndex(organization => organization.DisplayName);

            builder.Property(organization => organization.Id)
                .UseHiLo("organizationSequence");
            builder.Property(organization => organization.UniqueName)
                .IsRequired();

            builder.Property(organization => organization.Description);

            builder.HasMany(organization => organization.Quests)
                .WithOne()
                .HasForeignKey(x => x.OrganizationId);

            builder.HasMany(oo => oo.SliderPhotos).WithOne();

            //builder.OwnsMany(traveler => traveler.SliderPhotos,
            //    navigationBuilder => navigationBuilder.SetupSlider("TravelerFields", "TravelerId"));

        }
    }
}
