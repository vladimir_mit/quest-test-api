﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.SeedWork
{
    /// <summary>
    /// Базовый класс для сущностей
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        public int Id { get; protected set; }
    }
}
