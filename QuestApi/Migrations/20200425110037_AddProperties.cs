﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuestApi.Migrations
{
    public partial class AddProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Summary_MainPhotoRelevantUrl",
                table: "Quests",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GoogleMapSrc",
                table: "Organizations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Quests_Summary_MainPhotoRelevantUrl",
                table: "Quests",
                column: "Summary_MainPhotoRelevantUrl");

            migrationBuilder.AddForeignKey(
                name: "FK_Quests_SliderPhoto_Summary_MainPhotoRelevantUrl",
                table: "Quests",
                column: "Summary_MainPhotoRelevantUrl",
                principalTable: "SliderPhoto",
                principalColumn: "RelevantUrl",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quests_SliderPhoto_Summary_MainPhotoRelevantUrl",
                table: "Quests");

            migrationBuilder.DropIndex(
                name: "IX_Quests_Summary_MainPhotoRelevantUrl",
                table: "Quests");

            migrationBuilder.DropColumn(
                name: "Summary_MainPhotoRelevantUrl",
                table: "Quests");

            migrationBuilder.DropColumn(
                name: "GoogleMapSrc",
                table: "Organizations");
        }
    }
}
