﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Sites
{
    public class OrganizationRepository : BaseRepository<Organization>
    {
        public OrganizationRepository(SitesContext context) : base(context)
        {

        }
    }
}
