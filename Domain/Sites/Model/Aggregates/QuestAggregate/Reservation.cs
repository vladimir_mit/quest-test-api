﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.SeedWork;

namespace Domain.Sites.Model.Aggregates.QuestAggregate
{
    /// <summary>
    /// Бронирование
    /// </summary>
    public class Reservation : Entity
    {
        private Reservation() { }
        public Reservation(DateTime dateTime, ReservationStatus status, string participantName, string email, string phoneNumber, ReservationCostInformation costInformation, string participantComment)
        {
            DateTime = dateTime;
            Status = status;
            ParticipantName = participantName;
            Email = email;
            PhoneNumber = phoneNumber;
            CostInformation = costInformation;
            ParticipantComment = participantComment;
        }

        /// <summary>
        /// Дата и время бронирования
        /// </summary>
        public DateTime DateTime { get; private set; }

        /// <summary>
        /// Статус бронирования
        /// </summary>
        public ReservationStatus Status { get; private set; }

        /// <summary>
        /// Имя участника
        /// </summary>
        public string ParticipantName { get; private set; }

        /// <summary>
        /// Номер телефона для коммуникации
        /// </summary>
        public string PhoneNumber { get; private set; }

        /// <summary>
        /// Email для коммуникации
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Информация для формирования цены
        /// </summary>
        public ReservationCostInformation CostInformation { get; private set; }

        /// <summary>
        /// Цена
        /// </summary>
        /// <returns></returns>
        public int Cost { get; private set; }

        /// <summary>
        /// Комментарий от участника
        /// </summary>
        public string ParticipantComment { get; private set; }
    }
}
