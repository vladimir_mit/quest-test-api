﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Sites
{
    public class QuestRepository : BaseRepository<Quest>
    {
        public QuestRepository(SitesContext context) : base(context)
        {

        }
    }
}
