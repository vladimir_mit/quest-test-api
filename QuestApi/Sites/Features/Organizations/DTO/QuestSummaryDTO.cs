﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Sites.Model.Aggregates;

namespace QuestApi.Sites.Features.Organizations.DTO
{
    public class QuestSummaryDTO
    {
        /// <summary>
        /// Имя квеста
        /// </summary>
        public string QuestName { get; set; }

        /// <summary>
        /// Описание количества игроков
        /// </summary>
        public string PartiсipantsInfo { get; set; }

        /// <summary>
        /// Описание продолжительности квеста
        /// </summary>
        public string DurationInfo { get; set; }

        /// <summary>
        /// Описание стоимости квеста
        /// </summary>
        public string CostInfo { get; set; }

        /// <summary>
        /// Главное фото квеста 
        /// </summary>
        public SliderPhoto MainPhoto { get; set; }

    }
}
