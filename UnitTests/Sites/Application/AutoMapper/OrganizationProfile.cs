﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates;
using Domain.Sites.Model.Aggregates.CityAggregate;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using NUnit.Framework;
using QuestApi;
using QuestApi.Sites.Features.Organizations;

namespace UnitTests.Sites.Application.AutoMapper
{
    [TestFixture]
    class OrganizationProfile
    {
        /// <summary>
        ///     Маппер.
        /// </summary>
        protected IMapper Mapper { get; private set; }

        /// <summary>
        ///     Настраивает тесты.
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {
            var services = new ServiceCollection();
            services.AddAutoMapper(typeof(Startup).Assembly);

            Mapper = services.BuildServiceProvider().GetService<IMapper>();
        }

        [Test]
        public void Should_map_Organization_to_response()
        {
            var quest = new Quest(1, 
                new QuestSummary("Test", "2-4", "60", "200", 
                    new SliderPhoto("/test")),"desc", 
                new City("000", "tst", "+3", 
                    new List<Quest>()), new List<SliderPhoto>() { new SliderPhoto("/tst")});

            var org = new Organization("cdesc", "u", "dn", new List<SliderPhoto>() {new SliderPhoto("/t")},
                new List<Quest>() {quest}, "/src");

            var result = Mapper.Map<GetById.Response>(org);
        }
    }
}
