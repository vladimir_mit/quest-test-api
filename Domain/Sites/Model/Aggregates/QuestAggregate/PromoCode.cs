﻿using System.Reflection.Metadata;
using Domain.SeedWork;

namespace Domain.Sites.Model.Aggregates.QuestAggregate
{
    /// <summary>
    /// Промокод
    /// </summary>
    public class PromoCode : Entity
    {
        /// <summary>
        /// Код
        /// </summary>
        public string Code { get; }

        /// <summary>
        /// Процент скидки
        /// </summary>
        public int DiscountPercent { get; }

        /// <summary>
        /// Флаг активности
        /// </summary>
        public bool isActive { get; }

        /// <summary>
        /// Флаг одноразовости
        /// </summary>
        public bool isSingleUse { get; }
    }
}