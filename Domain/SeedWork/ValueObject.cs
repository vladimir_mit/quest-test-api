﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.SeedWork
{
    /// <summary>
    /// Объект значения
    /// </summary>
    public abstract class ValueObject
    {
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;
            ValueObject valueObject = (ValueObject)obj;
            using (IEnumerator<object> enumerator1 = this.GetAtomicValues().GetEnumerator())
            {
                using (IEnumerator<object> enumerator2 = valueObject.GetAtomicValues().GetEnumerator())
                {
                    while (enumerator1.MoveNext() && enumerator2.MoveNext())
                    {
                        if (enumerator1.Current == null ^ enumerator2.Current == null || enumerator1.Current != null && !enumerator1.Current.Equals(enumerator2.Current))
                            return false;
                    }
                    return !enumerator1.MoveNext() && !enumerator2.MoveNext();
                }
            }
        }

        public override int GetHashCode()
        {
            return this.GetAtomicValues().Select<object, int>((Func<object, int>)(x => x == null ? 0 : x.GetHashCode())).Aggregate<int>((Func<int, int, int>)((x, y) => x ^ y));
        }

        public ValueObject GetCopy()
        {
            return this.MemberwiseClone() as ValueObject;
        }

        protected abstract IEnumerable<object> GetAtomicValues();

        protected static bool EqualOperator(ValueObject left, ValueObject right)
        {
            return !(left == null ^ right == null) && (left == null || left.Equals((object)right));
        }

        protected static bool NotEqualOperator(ValueObject left, ValueObject right)
        {
            return !ValueObject.EqualOperator(left, right);
        }
    }
}
