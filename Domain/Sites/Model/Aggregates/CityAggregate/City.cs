﻿using System.Collections.Generic;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates.QuestAggregate;

namespace Domain.Sites.Model.Aggregates.CityAggregate
{
    /// <summary>
    /// Город
    /// </summary>
    public class City : Entity
    {
        private readonly List<Quest> _quests;

        private City()
        {
            _quests = new List<Quest>();
        }

        public City(string postal, string name, string timeZone, ICollection<Quest> quests)
        {
            Postal = postal;
            Name = name;
            TimeZone = timeZone;
            _quests = new List<Quest>(quests);
        }

        public string Postal { get; private set; }

        public string Name { get; private set; }

        public string TimeZone { get; private set; }

        public ICollection<Quest> Quests => _quests;
    }
}
