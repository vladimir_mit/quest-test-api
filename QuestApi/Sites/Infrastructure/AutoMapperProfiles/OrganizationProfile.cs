﻿using System.Linq;
using AutoMapper;
using Domain.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using QuestApi.Sites.Features.Organizations;
using QuestApi.Sites.Features.Organizations.DTO;

namespace QuestApi.Sites.Infrastructure.AutoMapperProfiles
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<Organization, GetById.Response>()
                .ForMember(d => d.QuestSummaries, 
                    s => s.MapFrom(x => x.Quests.Select(y => y.Summary)));
            CreateMap<QuestSummary, QuestSummaryDTO>();
        }
    }
}
