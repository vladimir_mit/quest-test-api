﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;

namespace Domain.Sites.Services
{
    /// <summary>
    /// Сервис для поиска промокодов
    /// </summary>
    public interface IPromoCodeFindService
    {
        /// <summary>
        /// Найти
        /// </summary>
        /// <param name="questId"> Id квеста </param>
        /// <param name="code"> Код </param>
        /// <returns></returns>
        PromoCode Find(int questId, string code);
    }
}
