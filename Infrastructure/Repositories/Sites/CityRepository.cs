﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.CityAggregate;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Sites
{
    public class CityRepository : BaseRepository<City>
    {
        public CityRepository(SitesContext context) : base(context)
        {

        }
    }
}
