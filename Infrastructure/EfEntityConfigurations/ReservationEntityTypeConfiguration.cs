﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.EntityConfigurations
{
    class ReservationEntityTypeConfiguration : IEntityTypeConfiguration<Reservation>
    {
        public void Configure(EntityTypeBuilder<Reservation> builder)
        {
            builder.HasKey(reservation => reservation.Id);
            builder.HasIndex(reservation => reservation.PhoneNumber);
            builder.HasIndex(reservation => reservation.Email);

            builder.Property(reservation => reservation.Id)
                .UseHiLo("reservationSequence");
            builder.Property(reservation => reservation.PhoneNumber)
                .IsRequired();
            builder.Property(reservation => reservation.Status)
                .IsRequired();
            builder.Property(reservation => reservation.ParticipantName)
                .IsRequired();
            builder.Property(reservation => reservation.DateTime)
                .IsRequired();
            builder.OwnsOne(reservation => reservation.CostInformation);
        }
    }
}
