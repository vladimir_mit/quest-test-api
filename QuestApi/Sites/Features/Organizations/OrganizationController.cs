﻿using System.Threading.Tasks;
using Domain.Sites.Model.Aggregates.OrganizationAggregate;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace QuestApi.Sites.Features.Organizations
{
    [ApiController]
    [Route("organizations")]
    public class OrganizationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrganizationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Route("{id}")]
        public async Task<GetById.Response> Get(int id)
        {
            return await _mediator.Send(new GetById.Query(id));
        }
    }
}
