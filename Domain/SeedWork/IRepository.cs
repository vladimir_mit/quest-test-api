﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Domain.SeedWork
{
    /// <summary>
    /// Репозиторий сущности
    /// </summary>
    /// <typeparam name="T"> Сущность </typeparam>
    public interface IRepository<T> where T : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetQueryable();

        /// <summary>
        /// Взять по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetById(int id);

        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="entity"></param>
        T Add(T entity);

        /// <summary>
        /// Найти по условию
        /// </summary>
        /// <param name="predicate"> Условие </param>
        /// <returns></returns>
        ICollection<T> Find(Expression<Func<T, bool>> predicate);

        IRepository<T> Include<T2>(Expression<Func<T, IEnumerable<T2>>> include, params Expression<Func<T2, object>>[] thenIncludes);

        Task<T> GetByIdAsync(int id, CancellationToken cancellationToken);
    }
}
