﻿using System;
using System.Collections.Generic;
using Domain.SeedWork;

namespace Domain.Sites.Model.Aggregates.QuestAggregate
{
    /// <summary>
    /// Информация для расчёта цены 
    /// </summary>
    public class ReservationCostInformation : ValueObject
    {
        public ReservationCostInformation(int participantsCount, string dayOfWeekNumber, TimeSpan time)
        {
            ParticipantsCount = participantsCount;
            DayOfWeekNumber = dayOfWeekNumber;
            Time = time;
        }

        /// <summary>
        /// Количество участников
        /// </summary>
        public int ParticipantsCount { get; private set; }

        /// <summary>
        /// Номер дня недели
        /// </summary>
        public string DayOfWeekNumber { get; private set; }

        /// <summary>
        /// Время суток
        /// </summary>
        public TimeSpan Time { get; private set; }

        /// <summary>
        /// Промокод
        /// </summary>
        public PromoCode PromoCode { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new ArgumentException();
        }
    }
}