﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Sites.Model.Aggregates.QuestAggregate
{
    public enum ReservationStatus
    {
        /// <summary>
        /// Новое
        /// </summary>
        New,
        /// <summary>
        /// Подтверждено
        /// </summary>
        Confirmed,
        /// <summary>
        /// Отклонено организатором
        /// </summary>
        Declined,
        /// <summary>
        /// Отменено участником
        /// </summary>
        Canceled
    }
}
