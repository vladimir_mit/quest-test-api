﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Sites
{
    public class PromoCodeRepository : BaseRepository<PromoCode>
    {
        public PromoCodeRepository(SitesContext context) : base(context)
        {

        }
    }
}
