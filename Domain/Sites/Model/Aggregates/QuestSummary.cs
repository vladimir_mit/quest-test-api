﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.SeedWork;
using Domain.Sites.Model.Aggregates;

namespace Domain.Model.Aggregates.OrganizationAggregate
{
    /// <summary>
    /// Краткая информация о квесте 
    /// </summary>
    public class QuestSummary : ValueObject
    {
        private QuestSummary()
        {

        }

        public QuestSummary(SliderPhoto mainPhoto)
        {
            MainPhoto = mainPhoto;
        }

        public QuestSummary(
            string questName,
            string partiсipantsInfo,
            string durationInfo, 
            string costInfo, SliderPhoto mainPhoto)
        {
            QuestName = questName;
            PartiсipantsInfo = partiсipantsInfo;
            DurationInfo = durationInfo;
            CostInfo = costInfo;
            MainPhoto = mainPhoto;
        }

        /// <summary>
        /// Имя квеста
        /// </summary>
        public string QuestName { get; private set; }

        /// <summary>
        /// Описание количества игроков
        /// </summary>
        public string PartiсipantsInfo { get; private set; }

        /// <summary>
        /// Описание продолжительности квеста
        /// </summary>
        public string DurationInfo { get; private set; }

        /// <summary>
        /// Описание стоимости квеста
        /// </summary>
        public string CostInfo { get; private set; }

        /// <summary>
        /// Главное фото квеста 
        /// </summary>
        public SliderPhoto MainPhoto { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
