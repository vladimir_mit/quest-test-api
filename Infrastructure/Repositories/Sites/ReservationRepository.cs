﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Sites.Model.Aggregates.QuestAggregate;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories.Sites
{
    public class ReservationRepository : BaseRepository<Reservation>
    {
        public ReservationRepository(DbContext context) : base(context)
        {

        }
    }
}
