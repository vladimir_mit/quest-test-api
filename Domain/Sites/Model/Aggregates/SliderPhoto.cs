﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.SeedWork;

namespace Domain.Sites.Model.Aggregates
{
    public class SliderPhoto : ValueObject
    {
        public SliderPhoto(string relevantUrl)
        {
            RelevantUrl = relevantUrl;
        }

        public string RelevantUrl { get; private set; }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new NotImplementedException();
        }
    }
}
